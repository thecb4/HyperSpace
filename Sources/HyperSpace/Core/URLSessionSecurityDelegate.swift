//
//  URLSessionSecurityDelegate.swift
//  NowThis
//
//  Created by Cavelle Benjamin on 18-Dec-17 (51).
//  Copyright © 2018 The CB4. All rights reserved.
//

import Foundation

public protocol URLSessionChallengeHandler {
  
  func handle(
    _ session: URLSession, didReceive challenge: URLAuthenticationChallenge,
    completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void)
  
}

public protocol APISessionDelegateProtocol: URLSessionDelegate {
  var challengeHandlers: [String: URLSessionChallengeHandler] { get set }
}

// https://forums.developer.apple.com/thread/77694
// http://www.neglectedpotential.com/2015/06/working-with-apples-application-transport-security/
public class APISessionDelegate: NSObject, APISessionDelegateProtocol {
  
  public var challengeHandlers: [String : URLSessionChallengeHandler] = [String : URLSessionChallengeHandler]()

  public func urlSession(
    _ session: URLSession, didReceive challenge: URLAuthenticationChallenge, 
    completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
    
    if challengeHandlers.keys.contains(challenge.protectionSpace.host) {
      
      challengeHandlers[challenge.protectionSpace.host]?.handle(session, didReceive: challenge, completionHandler: completionHandler)
      
    } else {
      
      completionHandler(.performDefaultHandling, nil)
      
    }

  }

}
