
import Foundation

public protocol APIProtocol {
  
  associatedtype Environment: EnvironmentProtocol
  associatedtype Route: RouteProtocol, URLSessionTaskable
  
  static var current: Environment { get }

}