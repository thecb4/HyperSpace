//
//  Session.swift
//  NowThis
//
//  Created by Cavelle Benjamin on 18-Oct-05 (40).
//  Copyright © 2018 The CB4. All rights reserved.
//

import Foundation

public typealias DataTaskCompletionHandler = (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void

public protocol URLSessionProtocol {
  
  var configuration: URLSessionConfiguration { get }
  
  var delegate: URLSessionDelegate? { get }
  
  var delegateQueue: OperationQueue { get }
  
  func dataTask(with url: URL, completionHandler: @escaping DataTaskCompletionHandler) -> URLSessionTaskProtocol
  
  func dataTask(with request: URLRequest, completionHandler: @escaping DataTaskCompletionHandler) -> URLSessionTaskProtocol
  
}

public protocol URLSessionTaskProtocol {
  func resume()
  func cancel()
}

extension URLSessionTask: URLSessionTaskProtocol {}

extension URLSession: URLSessionProtocol {
  
  public func dataTask(with url: URL, completionHandler: @escaping DataTaskCompletionHandler) -> URLSessionTaskProtocol {
    
    // specify the return type to avoid ambiguity in what is being called.
    let task: URLSessionDataTask = dataTask(with: url, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
      
      completionHandler(data, response, error)
      
    }) as URLSessionDataTask
    
    return task
    
  }
  
  public func dataTask(with request: URLRequest, completionHandler: @escaping DataTaskCompletionHandler) -> URLSessionTaskProtocol {
    
    // specify the return type to avoid ambiguity in what is being called.
    let task: URLSessionDataTask = dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
      
      completionHandler(data, response, error)
      
    }) as URLSessionDataTask
    
    return task
    
  }
  
}
