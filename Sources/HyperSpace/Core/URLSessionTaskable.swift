import Foundation
// https://medium.com/flawless-app-stories/writing-network-layer-in-swift-protocol-oriented-approach-4fa40ef1f908
public struct URLSessionTaskKey {
  public var name: String

  public init(name: String) {
    self.name = name
  }
}

extension URLSessionTaskKey: Equatable, Hashable, RawRepresentable {
  
  public typealias RawValue = String
  
  public init?(rawValue: URLSessionTaskKey.RawValue) {
    self.name = rawValue
  }
  
  public var rawValue: String {
    return name
  }
  
}

public protocol URLSessionTaskable {
  var taskKey: URLSessionTaskKey { get }
}