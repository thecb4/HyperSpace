//
//  MockURLSession.swift
//  NowThisTests
//
//  Created by Cavelle Benjamin on 18-Sep-27 (39).
//  Copyright © 2018 The CB4. All rights reserved.
//

import Foundation

public struct MockNetworkInfo {
  
  public var urlString: String
  public var responseBody: Data?
  public var mimeType: String
  public var textEncodingName: String
  public var error: Error?
  public var request: URLRequest?
  
  public var response: URLResponse {
    return URLResponse(
      url: URL(string: urlString)!,
      mimeType: self.mimeType,
      expectedContentLength: self.responseBody?.count ?? 0,
      textEncodingName: self.textEncodingName)
  }
  
  public var url: URL? {
    
    return URL(string: urlString)
    
  }

  public init(urlString: String, responseBody: Data?, mimeType: String, textEncodingName: String, error: Error?, request: URLRequest?) {
    self.urlString = urlString
    self.responseBody = responseBody
    self.mimeType = mimeType
    self.textEncodingName = textEncodingName
    self.error = error
    self.request = request
  }
  
}

extension MockNetworkInfo: Equatable {
  public static func == (lhs: MockNetworkInfo, rhs: MockNetworkInfo) -> Bool {

    return
      lhs.urlString    == rhs.urlString &&
      lhs.responseBody == rhs.responseBody &&
      lhs.mimeType     == rhs.mimeType &&
      lhs.textEncodingName == rhs.textEncodingName &&
      (lhs.error?.localizedDescription ?? "") == (rhs.error?.localizedDescription ?? "") &&
      lhs.request      == rhs.request &&
      lhs.url          == rhs.url &&
      lhs.response.url      == rhs.response.url &&
      lhs.response.mimeType == rhs.response.mimeType &&
      lhs.response.expectedContentLength == rhs.response.expectedContentLength &&
      lhs.response.textEncodingName      == rhs.response.textEncodingName &&
      lhs.response.suggestedFilename     == rhs.response.suggestedFilename
    
    
  }
  
  
}

extension URL {
  
  public var request: URLRequest? {
    
    return URLRequest(url: self)
    
  }
  
}

public class MockURLSession: URLSessionProtocol {
  
  public var delegate: URLSessionDelegate?
  
  
  public var configuration: URLSessionConfiguration
  
  public var delegateQueue: OperationQueue
  
  private (set) var lastURL: URL? = nil
  private (set) var lastRequest: URLRequest? = nil
  
  public var info: MockNetworkInfo
  
  public init(info: MockNetworkInfo, configuration: URLSessionConfiguration = URLSessionConfiguration.default, delegateQueue: OperationQueue = .main) {
    
    self.info = info
    self.configuration = configuration
    self.delegateQueue = delegateQueue
    
  }
  
  public var request: URLRequest? {
    
    return self.info.request
    
  }
  
  // https://stackoverflow.com/questions/49508230/how-to-stub-urlsession-in-swift
  public func dataTask(with url: URL, completionHandler: @escaping DataTaskCompletionHandler) -> URLSessionTaskProtocol {
    
    self.info.urlString = url.absoluteString
    
    self.info.request   = info.url?.request
    
    let task = MockDataTask(data: info.responseBody, response: self.info.response, error: info.error)
    
    task.completionHandler = completionHandler
    
    return task
    
  }
  
  public func dataTask(with request: URLRequest, completionHandler: @escaping DataTaskCompletionHandler) -> URLSessionTaskProtocol {
    
    self.info.request = request
    
    let task = MockDataTask(data: info.responseBody, response: self.info.response, error: info.error)
    
    task.completionHandler = completionHandler
    
    return task
    
  }
  
}
