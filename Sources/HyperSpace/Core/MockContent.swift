//
//  MockContent.swift
//  NowThis
//
//  Created by Cavelle Benjamin on 18-Oct-13 (41).
//  Copyright © 2018 The CB4. All rights reserved.
//

import Foundation
import DarkMatter

public struct MockContent: DarkMatter {
  public var name: String
  
  public init() {
    name = ""
  }

  public init(name: String) {
    self.name = name
  }
  
}
