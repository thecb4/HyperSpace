
import Foundation

public protocol RouteProtocol {
  var path: String { get }
  var httpMethod: String { get }
  var queryItems: [URLQueryItem]? { get }
  var body: Data? { get }
  var headers: [String: String]? { get }
  var requestCachePolicy: URLRequest.CachePolicy { get }
  var requestTimeoutInterval: TimeInterval { get }
  
  func mockHttpDataResponse(for statusCode: Int) -> Data?
}