//
//  Endpoint.swift
//  NowThis
//
//  Created by Cavelle Benjamin on 18-Oct-05 (40).
//  Copyright © 2018 The CB4. All rights reserved.
//

import Foundation
// https://medium.com/flawless-app-stories/writing-network-layer-in-swift-protocol-oriented-approach-4fa40ef1f908

public struct EndPoint<API: APIProtocol> : URLRepresentable, URLRequestRepresentable {
  
  public let environment: API.Environment
  public let route: API.Route
  
  public init(_ environment: API.Environment = API.current, at route: API.Route) {
    self.environment = environment
    self.route = route
  }
  
  public var components: URLComponents {
    
    var components = URLComponents()
    
    components.scheme     = self.environment.value.scheme.rawValue
    components.host       = self.environment.value.host
    components.port       = self.environment.value.port
    components.path       = self.route.path
    components.queryItems = self.route.queryItems?.isEmpty ?? false ? nil : self.route.queryItems
    
    return components
  }
  
  public var url: URL? {
    
    return components.url
    
  }
  
  public var request: URLRequest? {
    
    guard let url = self.url else { return nil }
    
    var request = URLRequest(
      url: url,
      cachePolicy: self.route.requestCachePolicy,
      timeoutInterval: self.route.requestTimeoutInterval
    )
    
    request.httpMethod = self.route.httpMethod
    
    if let headers = self.route.headers { request.allHTTPHeaderFields = headers }
    
    if let body    = self.route.body { request.httpBody = body }
    
    return request
    
  }
  
}
