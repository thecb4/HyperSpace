//
//  MockDataTask.swift
//  NowThisTests
//
//  Created by Cavelle Benjamin on 18-Oct-01 (40).
//  Copyright © 2018 The CB4. All rights reserved.
//

import Foundation

public class MockDataTask: URLSessionTaskProtocol {
  
  public var completionHandler: DataTaskCompletionHandler?
  
  public var data: Data?
  public var response: URLResponse?
  public var error: Error?
  
  public init(data: Data?, response: URLResponse?, error: Error?) {
    self.data     = data
    self.response = response
    self.error    = error
  }
  
  public func cancel() {}
  public func resume() {
    completionHandler?(data, response, error)
  }
}
