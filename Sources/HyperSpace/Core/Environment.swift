
import Foundation

public protocol EnvironmentProtocol: URLSessionChallengeHandler {
  var value: URL.Env { get }
}

extension URL {

  public struct Env {
    
    public enum Scheme: String {
      case http
      case https
    }
    
    public var scheme: Scheme
    public var host: String
    public var port: Int?
    
    public static func localhost(_ scheme: Scheme, _ port: Int? = nil) -> Env {
      return Env(scheme, "localhost", port)
    }
    
    public static func localhost(_ port: Int? = nil) -> Env {
      return localhost(.http, port)
    }
    
    public init(_ scheme: Scheme, _ host: String, _ port: Int? = nil) {
      self.scheme = scheme
      self.host = host
      self.port = port
    }
    
  }
}

extension URL.Env: Equatable {}

extension URL.Env: EnvironmentProtocol {
  public func handle(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
    completionHandler(.performDefaultHandling, nil)
  }
  
  public var value: URL.Env {
    return self
  }
}

public protocol URLRepresentable {
  var components: URLComponents { get }
  var url: URL? { get }
  
}

public protocol URLRequestRepresentable {
  var request: URLRequest? { get }
}
