//
//  NowThisTests.swift
//  NowThisTests
//
//  Created by Cavelle Benjamin on 18-Aug-30 (35).
//  Copyright © 2018 The CB4. All rights reserved.
//

import XCTest
import HyperSpace
import DarkMatter

class NetworkingTests: XCTestCase {
    
  override func setUp() {
      super.setUp()
      // Put setup code here. This method is called before the invocation of each test method in the class.
  }
  
  override func tearDown() {
      // Put teardown code here. This method is called after the invocation of each test method in the class.
      super.tearDown()
  }

  func testMockAPIShouldHaveCurrentAsLocalHost() {

    let expected = MockAPI.Environment.localhost

    let actual  = MockAPI.current

    XCTAssertEqual(expected, actual)

  }
  
  
  func testCreateMockServiceRequest() {
    
    MockAPI.current = .localhost
    
    var expected = URLRequest(
      url: URL(string: "https://hello.localhost/world")!,
      cachePolicy: .useProtocolCachePolicy,
      timeoutInterval: 10.0
    )
    
    expected.allHTTPHeaderFields = ["Content-Type": "application/json;charset=utf-8"]
    
    expected.httpMethod = "GET"
    
    let endPoint = EndPoint<MockAPI>(at: .hello("123456"))
    
    guard let actual = endPoint.request else { return }
    
    XCTAssertEqual(actual, expected)
    
  }
  
  func testChangeCurrentEnvironment() {
    
    var expected = URLRequest(
      url: URL(string: "https://hello.localhost/world")!,
      cachePolicy: .useProtocolCachePolicy,
      timeoutInterval: 10.0
    )
    
    expected.allHTTPHeaderFields = ["Content-Type": "application/json;charset=utf-8"]
    
    expected.httpMethod = "GET"
    
    var endPoint = EndPoint<MockAPI>(at: .hello("123456"))
    
    guard let actual1 = endPoint.request else { return }
    
    XCTAssertEqual(actual1, expected)
    
    MockAPI.current = .production
    
    expected = URLRequest(
      url: URL(string: "https://hello.com/world")!,
      cachePolicy: .useProtocolCachePolicy,
      timeoutInterval: 10.0
    )
    
    expected.allHTTPHeaderFields = ["Content-Type": "application/json;charset=utf-8"]
    
    expected.httpMethod = "GET"
    
    endPoint = EndPoint<MockAPI>(at: .hello("123456"))
    
    guard let actual2 = endPoint.request else { return }
    
    XCTAssertEqual(actual2, expected)
    
  }
  
  func testMockNetworkingHttpRequest() {
    
    let url = URL(string: "http://world")!
    
    let data = "".data(using: .utf8)!
    
    let expected = MockNetworkInfo(
      urlString: url.absoluteString,
      responseBody: data,
      mimeType: "text/html",
      textEncodingName: "utf-8",
      error: nil,
      request: url.request
    )
    
    let response = URLResponse(
      url: url,
      mimeType: "text/html",
      expectedContentLength: data.count,
      textEncodingName: "utf-8")
    
    let mock = MockConnection(data, response, nil)
    
    let request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
    
    let expectation = self.expectation(description: "Download Now Playing")
    
    let service = ServiceManager.shared
    
    service.make(mock: mock, request, for: URLSessionTaskKey(name: "searchMovies")) { (data, response, error) in
      
      let actual = MockNetworkInfo(
        urlString: response?.url?.absoluteString ?? "",
        responseBody: data,
        mimeType: response?.mimeType ?? "" ,
        textEncodingName: response?.textEncodingName ?? "",
        error: error,
        request: url.request
      )

      XCTAssertEqual(actual, expected)

      expectation.fulfill()
    }
    
    waitForExpectations(timeout: 10, handler: nil)
    
  }
  
  func testServiceCallWithMockSessionAndNotNilResponseBody() {

    
    let endPoint = EndPoint<MockAPI>(.mock, at: .hello("123456"))
    
    let data = "{ \"name\": \"Happy\"}".data(using: .utf8)!
    
    let response = URLResponse(
      url: endPoint.url!,
      mimeType: "text/html",
      expectedContentLength: data.count,
      textEncodingName: "utf-8")
    
    let mock = MockConnection(data, response, nil)
    
    let service = ServiceManager.shared
    
    let expectation = self.expectation(description: "testMockServiceCallNilResponseBody")
    
    guard let expectedContent = try? JSONDecoder().decode(MockContent.self, from: "{ \"name\": \"Happy\"}".data(using: .utf8)!) else {
      XCTFail("Cannot decode \(MockContent.self)")
      return
    }
    
    service.connect(mock: mock, to: endPoint, returning: MockContent.self) { (content) in
      
      guard let content = content else {
        XCTFail()
        return
      }
      
      XCTAssertEqual(content, expectedContent)
      
      expectation.fulfill()
      
    }
    
    waitForExpectations(timeout: 10, handler: nil)
    
  }
  
  func testServiceCallWithMockConnectionAndNotNilResponseBody() {
    
    let endPoint = EndPoint<MockAPI>(at: .hello("123456"))
    
    let service = ServiceManager.shared
    
    let expectation = self.expectation(description: "testMockServiceCallNilResponseBody")
    
    let data = "{ \"name\": \"Happy\"}".data(using: .utf8)
    
    let response = URLResponse(
      url: endPoint.url!,
      mimeType: "text/html",
      expectedContentLength: 10,
      textEncodingName: nil)
    
    guard let expectedContent = try? JSONDecoder().decode(MockContent.self, from: data!) else {
      XCTFail("Cannot decode \(MockContent.self)")
      return
    }
    
    service.connect(mock: (data, response, nil), to: endPoint, returning: MockContent.self) { (content) in
      
      guard let content = content else {
        XCTFail()
        return
      }
      
      XCTAssertEqual(content, expectedContent)
      
      expectation.fulfill()
      
    }
    
    waitForExpectations(timeout: 10, handler: nil)
    
  }

  func testServiceWillMockServiceStatusCodeReturn() {

    let endPoint = EndPoint<MockAPI>(at: .hello("123456"))

    let expectedCode = 200

    let response = HTTPURLResponse(
        url: endPoint.url!,
        statusCode: expectedCode,
        httpVersion: "1.1",
        headerFields: nil
      )

    let service = ServiceManager.shared

    let expectation = self.expectation(description: "return status code")

    service.connect(mock: (nil, response, nil), to: endPoint) { (code: Int?) in

      guard let actualCode = code else {
        XCTFail()
        return
      }

      XCTAssertEqual(actualCode, expectedCode)

      expectation.fulfill()
      
    }
    
    waitForExpectations(timeout: 10, handler: nil)   

  }

  static var allTests = [
    ("testMockAPIShouldHaveCurrentAsLocalHost", testMockAPIShouldHaveCurrentAsLocalHost),
    ("testCreateMockServiceRequest", testCreateMockServiceRequest),
    ("testChangeCurrentEnvironment", testChangeCurrentEnvironment),
    ("testMockNetworkingHttpRequest", testMockNetworkingHttpRequest),
    ("testServiceCallWithMockSessionAndNotNilResponseBody", testServiceCallWithMockSessionAndNotNilResponseBody),
    ("testServiceCallWithMockConnectionAndNotNilResponseBody", testServiceCallWithMockConnectionAndNotNilResponseBody),
    ("testServiceCallWithMockConnectionAndNotNilResponseBody", testServiceCallWithMockConnectionAndNotNilResponseBody),
    ("testServiceWillMockServiceStatusCodeReturn", testServiceWillMockServiceStatusCodeReturn)

  ]
    
}
