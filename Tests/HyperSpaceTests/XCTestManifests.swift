import XCTest

#if os(Linux)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(HyperSpaceTests.allTests),
        testCase(NetworkingTests.allTests)
    ]
}
#endif
