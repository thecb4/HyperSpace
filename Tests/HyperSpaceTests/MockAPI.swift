//
//  TestAPI.swift
//  NowThisTests
//
//  Created by Cavelle Benjamin on 18-Oct-05 (40).
//  Copyright © 2018 The CB4. All rights reserved.
//

import Foundation
import HyperSpace

extension URLSessionTaskKey {
  public static let hello = URLSessionTaskKey(name: "hello")
}

struct MockAPI: APIProtocol {
  
  static var current: Environment = .localhost

  enum Environment: EnvironmentProtocol {
    
    case mock
    case localhost
    case production

    var value: URL.Env {
      switch self {
        case .localhost: return URL.Env(.https, "hello.localhost")
        case .mock: return URL.Env(.http, "hello.mock")
        case .production: return URL.Env(.https, "hello.com")
      }
    }
    
    func handle(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
      
      #if os(Linux)
        completionHandler(.performDefaultHandling, nil)
      #else 
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
      #endif
  
    }
    
  }
  
  enum Route: RouteProtocol, URLSessionTaskable {
    
    case hello(String)
    
    var requestCachePolicy: URLRequest.CachePolicy {
      switch self {
      case .hello:
        return .useProtocolCachePolicy
      }
    }
    
    var requestTimeoutInterval: TimeInterval {
      switch self {
      case .hello:
        return 10.0
      }
    }
    
    var taskKey: URLSessionTaskKey {
      switch self {
      case .hello:
        return .hello
      }
    }
    
    var path: String {
      switch self {
      case .hello:   
        return "/world"
      }
    }
    
    var queryItems: [URLQueryItem]? {
      switch self {
      case .hello:
        return []
      }
    }
    
    var headers: [String: String]? {
      switch self {
      case .hello:
        return [ "Content-Type": "application/json;charset=utf-8" ]
      }
    }
    
    var httpMethod: String {
      switch self {
      case .hello:   
        return "GET"
      }
    }
    
    var body: Data? {
      switch self {
      case .hello:
        return nil
      }
    }
    
    func mockHttpDataResponse(for statusCode: Int) -> Data? {
      switch self {
        case .hello:
          return try? MockContent(name: "Happy").encoded()
      }
    }
    
  }
  
}
