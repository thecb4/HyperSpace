import XCTest

import HyperSpaceTests

var tests = [XCTestCaseEntry]()
tests += HyperSpaceTests.allTests()
XCTMain(tests)
