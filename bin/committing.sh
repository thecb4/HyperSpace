#!/usr/bin/env bash

echo "=============== Linting =============== " && \
swiftlint && \
echo "=============== Building Dependencies =============== " && \
carthage update --platform ios && \
echo "=============== Running Tests =============== " && \
bin/test.sh && \
echo "=============== Updating Docs =============== " && \
bin/docs.sh && \
echo "=============== Adding files to git =============== " && \
git add . && \
echo "=============== Commiting files to git =============== " && \
git commit -m "$1"

# carthage build --no-skip-current --platform ios && \
# carthage archive HyperSpace