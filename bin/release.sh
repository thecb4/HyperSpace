#!/usr/bin/env bash

echo "=============== Linting =============== " && \
swiftlint && \
echo "=============== Running Tests =============== " && \
bin/test.sh && \
echo "=============== Updating Docs =============== " && \
bin/docs.sh && \
echo "=============== Packaging Carthage Binary =============== " && \
carthage build --no-skip-current --platform ios && \
carthage archive HyperSpace && \
echo "=============== Adding files to git =============== " && \
git add . && \
echo "=============== Commiting files to git =============== " && \
git commit -m "$1"
