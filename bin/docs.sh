#!/usr/bin/env bash

sourcekitten doc --spm-module HyperSpace > source.json && \
jazzy && \
rm source.json
