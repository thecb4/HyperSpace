# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.2.2] - 2019-Jan-09
### Added
- 

### Changed (not fixed)
- 

### Removed
- 

### Fixed
- gitlab-ci.yml to use shared runner


## [0.2.1] - 2019-Jan-09
### Added
- CHANGELOG.md
- LICENSE.md
- CONTRIBUTING.md
- Service return http status code
- gitlab-ci.yml for gitlab doc pages

### Changed (not fixed)
- Tests to disambiguate calls between Data and Status Code

### Removed
- Debug print statements in service

### Fixed
- Linux tests to include NetworkingTests