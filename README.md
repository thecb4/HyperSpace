# HyperSpace
A fork of [SweetRouter](https://github.com/alickbass/SweetRouter)


## Integration


### Carthage

You can use Carthage to install SweetRouter by adding it to your Cartfile:

```swift
.package(url: "https://gitlab.com/thecb4/HyperSpace.git", .upToNextMinor(from: "0.2.1"))
```

### Swift Package Manager

You can use Carthage to install SweetRouter by adding it to your Cartfile:

```swift
gitlab "thecb4/HyperSpace"
```

## Definitions

Imagine that you use the following `URLs` within your App

```
https://myservercom.com:123/api/new/signIn
https://myservercom.com:123/api/new/signOut
https://myservercom.com:123/api/new/posts?date=today&userId=id
```

Every `URL` in the list is called an **`Endpoint`**

### Endpoint

Endpoint has the following structure:

```
Endpoint
┌─────────────────────────────────┴────────────────────────────────────┐
https://myservercom.com:123/api/new/posts?date=today&userId=id#paragraph
└────────────────┬────────────────┘└────────────────┬──────────────────┘
Environment                           Route
```

Endpoint is represented with `EndpointType` `protocol`.

### Environment

Environment has the following structure:

```
Environment
┌────────────────┴─────────────────┐
https://myservercom.com:123/api/new/posts?date=today&userId=id#paragraph
└─┬─┘  └───────┬───────┘└┬┘└─────┬─┘
scheme        host     port default path
```

**Examples of Environment**

```swift
URL.Env(.https, "mytestserver.com").at("api", "new") // https://mytestserver.com/api/new/
URL.Env(IP(127, 0, 0, 1), 8080) // http://127.0.01:8080
URL.Env.localhost(4001) // http://localhost:4001
```

### Route
Route has the following structure:

```
Route
┌─────────────────┴─────────────────┐
https://myservercom.com:123/api/new/posts?date=today&userId=id#paragraph
└──┬──┘└────────┬─────────┘└───┬────┘
path        query         fragment
```

**Example of Route**

```swift
// /api/new/posts?date=today&userId=id#paragraph
URL.Route(at: "api", "new", "posts").query(("date", "today"), ("userId", "id")).fragment("paragraph")
```

## Example of usage

Here is an example of the Router for some back-end `API`:

```swift
struct MockAPI: APIProtocol {
  
  static var current: Environment = .localhost

  enum Environment: EnvironmentProtocol {
    
    case mock
    case localhost
    case production

    var value: URL.Env {
      switch self {
        case .localhost: return URL.Env(.https, "hello.localhost")
        case .mock: return URL.Env(.http, "hello.mock")
        case .production: return URL.Env(.https, "hello.com")
      }
    }
    
    func handle(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
      
      #if os(Linux)
        completionHandler(.performDefaultHandling, nil)
      #else 
        completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
      #endif
  
    }
    
  }
  
  enum Route: RouteProtocol, URLSessionTaskable {
    
    case hello(String)
    
    var requestCachePolicy: URLRequest.CachePolicy {
      switch self {
      case .hello:
        return .useProtocolCachePolicy
      }
    }
    
    var requestTimeoutInterval: TimeInterval {
      switch self {
      case .hello:
        return 10.0
      }
    }
    
    var taskKey: URLSessionTaskKey {
      switch self {
      case .hello:
        return .hello
      }
    }
    
    var path: String {
      switch self {
      case .hello:   
        return "/world"
      }
    }
    
    var queryItems: [URLQueryItem]? {
      switch self {
      case .hello:
        return []
      }
    }
    
    var headers: [String: String]? {
      switch self {
      case .hello:
        return [ "Content-Type": "application/json;charset=utf-8" ]
      }
    }
    
    var httpMethod: String {
      switch self {
      case .hello:   
        return "GET"
      }
    }
    
    var body: Data? {
      switch self {
      case .hello:
        return nil
      }
    }
    
    func mockHttpDataResponse(for statusCode: Int) -> Data? {
      switch self {
        case .hello:
          return try? MockContent(name: "Happy").encoded()
      }
    }
    
  }
  
}
```

## Resolving Endpoints with a service
Endpoints resolve with a service. A service may return an http status code, data, or specific content.

### Returning content
```swift
  service.connect(to: endPoint, returning: SomeContent.self) { (content) in
    
    guard let content = content else {
      // do work when it fails
    }
    
    // work with content
    
  }
```

### Returning data
```swift
  service.connect(to: endPoint) { (data: Data?) in
    
    guard let data = data else {
      // do work when it fails
    }
    
    // work with data
    
  }
```

### Returning status code
```swift
  service.connect(to: endPoint) { (code: Int?) in
    
    guard let data = data else {
      // do work when it fails
    }
    
    // work with data
    
  }
```

## Mocking

Sometimes you need to mock your end point. Since the EndPoint protocol has mocking built in, you can leverage that in your calls.

```swift
service.connect(mock: (data, response, nil), to: endPoint, returning: MockContent.self) { (content) in
  
  guard let content = content else {
    XCTFail()
    return
  }
  
  XCTAssertEqual(content, expectedContent)
  
  expectation.fulfill()
  
}
```

As easy as that 😉
